Пример "Меню деталей" React + Redux

При выборе пункта меню-детали, отображается: наименование, фото, описание

![Alt text](https://bytebucket.org/Sobolev_Sergey/javascript-react-redux-example-menu/raw/11bc09d6ba82e9f75c35c94a0012aa9521082e99/menu1.png?token=aa3834169111cde0e0b2ebfab81e472e0e646133)

![Alt text](https://bytebucket.org/Sobolev_Sergey/javascript-react-redux-example-menu/raw/11bc09d6ba82e9f75c35c94a0012aa9521082e99/menu2.png?token=31783ae1815d47311dad0c70daee05cb50aada59)

![Alt text](https://bytebucket.org/Sobolev_Sergey/javascript-react-redux-example-menu/raw/11bc09d6ba82e9f75c35c94a0012aa9521082e99/menu3.png?token=032c28be856b93c3aa8d14a48747fbb022d89f30)