import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {selectMenu} from "../actions/index";

class MenuList extends Component {
    showList () {
        return this.props.menu.map((menu) => {
            return (
              <li onClick={() => this.props.select (menu)}
                  key={menu.id}>{menu.title}</li>
            );
        });
    }

    render (){
        return(
            <ol>
                {this.showList()}
            </ol>
        );
    }
}

function mapStateToProps(state) {
    return {
        menu: state.menu
    };
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({select: selectMenu}, dispatch)
}

export default connect(mapStateToProps,
    matchDispatchToProps)(MenuList);