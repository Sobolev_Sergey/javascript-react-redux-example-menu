import React, {Component} from 'react';
import {connect} from 'react-redux';

class Details extends Component {
    render () {
        if (!this.props.menu) {
            return (<p>Выберите деталь..</p>)
        }
        return (
            <div>
                <h2>{this.props.menu.title}</h2>
                <img src={this.props.menu.img}/><br />
                <p>{this.props.menu.desc}</p>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        menu: state.active
    };
}

export default connect (mapStateToProps)(Details);