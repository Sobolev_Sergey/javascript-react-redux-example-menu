import {combineReducers} from 'redux';
import MenuReducers from './menu';
import ActiveMenu from './menu-active';

const allReducers = combineReducers({
    menu: MenuReducers,
    active: ActiveMenu
});

export default allReducers;