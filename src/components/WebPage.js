import React from 'react';
import MenuList from '../containers/menu-list';
import Details from '../containers/details';

const WebPage = () => (
  <div>
      <h2> Меню деталей: </h2>
      <MenuList/>
      <hr></hr>
      <h3> Характеристики: </h3>
      <Details />
  </div>
);

export default WebPage;